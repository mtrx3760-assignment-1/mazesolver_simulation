# Maze Solver Package

### Steps to run the Simulation:

(New Terminal)
- `cd ~/catkin_ws/src`    
- `git clone https://hrithiksp@bitbucket.org/mtrx3760-assignment-1/mazesolver_simulation.git`    
- `cd ~/catkin_ws`   
- `catkin_make`    
- `.~/catkin_ws/devel/setup.bash`  
- `roscore`    

(New Terminal)   
- load turtlebot3 burger `export TURTLEBOT3_MODEL=burger`  
- Open the simulation `roslaunch mazesolver_simulation enclosed_maze.launch`   

(New Terminal)
- Run the maze solver `roslaunch mazesolver_simulation turtlebot3_mazesolve.launch`
